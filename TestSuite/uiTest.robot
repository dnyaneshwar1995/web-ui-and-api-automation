*** Settings ***

Resource                ${EXECDIR}/Resource/Common.resource
Suite Setup             UI Suite Setup
Suite Teardown          UI Suite Teardown
Force Tags              uitest
Documentation           This suite contains UI related test cases
...                     Force tag given for this suite: uitest

*** Test Cases ***

Register and login as a user
    [Documentation]    This test case registers a new user and login to the app
    Go To Registration Page
    Input Username    ${usernameUI}
    Input Password    ${passwordUI}
    Input First Name    ${firstNameUI}
    Input Family Name    ${familyNameUI}
    Input Phone Number    ${phoneNumberUI}
    Wait Until Element Is Clickable And Click    ${registerSubmitButton}
    Go To Home Page
    Go To Login Page
    Input Username    ${usernameUI}
    Input Password    ${passwordUI}
    Click On Lgin Button
    Validate User Information    ${usernameUI}    ${firstNameUI}    ${familyNameUI}    ${phoneNumberUI}
    [Teardown]    Logout From App