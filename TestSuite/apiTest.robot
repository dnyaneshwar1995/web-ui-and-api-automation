*** Settings ***

Resource                ${EXECDIR}/Resource/Common.resource
Suite Setup             API Suite Setup
Force Tags              apitest
Documentation           This suite contains api related test cases
...                     Force tag given for this suite: apitest

*** Test Cases ***

Review users registered in system
    ${usersList}    Review Users    ${token}
    Should Contain    ${usersList}    ${username}

Get personal information of users with authentication
    ${userDataDict}    Get Personal Information Of User    ${token}    ${username}
    Dictionary Should Contain Item    ${userDataDict}    ${firstNameKey}    ${firstName}
    Dictionary Should Contain Item    ${userDataDict}    ${lastNameKey}    ${familyName}
    Dictionary Should Contain Item    ${userDataDict}    ${phoneKey}    ${phoneNumber}

Update personal information of users with authentication
    ${json_string}=    evaluate    json.loads('''${updateDataDict}''')    json
    ${respDict}    Update Personal Information Of User    ${token}    ${username}    ${json_string}
    Dictionary Should Contain Item    ${respDict}    ${updateMessageKey}    ${updatedValue}
    Dictionary Should Contain Item    ${respDict}    ${updateStatusKey}    ${updatedSuccessKey}
    ${userDataDict}    Get Personal Information Of User    ${token}    ${username}
    ${json_string}    Convert To Dictionary    ${userDataDict}
    ${val1}    Get From Dictionary    ${json_string}    ${firstNameKey}
    ${val2}    Get From Dictionary    ${json_string}    ${phoneKey}
    Dictionary Should Contain Item    ${json_string}    ${firstNameKey}    ${val1}
    Dictionary Should Contain Item    ${json_string}    ${lastNameKey}    ${familyName}
    Dictionary Should Contain Item    ${json_string}    ${phoneKey}    ${val2}

