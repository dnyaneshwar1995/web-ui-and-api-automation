*** Settings ***

Resource                ${EXECDIR}/Resource/Common.resource

*** Keywords ***

Wait Until Element Is Clickable And Click
    [Documentation]    This keyword waits until element can be clicked and clicks it
    ...                It takes one argument, element to click
    [Arguments]    ${elementToClick}
    Wait Until Page Contains Element    ${elementToClick}
    Wait Until Keyword Succeeds    5s    500ms    Click Element    ${elementToClick}