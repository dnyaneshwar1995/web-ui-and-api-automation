*** Settings ***

Resource                ${EXECDIR}/Resource/Common.resource

*** Keywords ***

Go To Home Page
    Wait Until Element Is Clickable And Click    ${demoAppHomeButton}

Go To Registration Page
    Wait Until Element Is Clickable And Click    ${registerButton}
    Wait Until Page Contains Element    ${registerHeader}

Go To Login Page
    Wait Until Element Is Clickable And Click    ${loginButton}
    Wait Until Page Contains Element    ${loginHeader}

Input Username
    [Arguments]    ${usernameToInput}
    Wait Until Page Contains Element    ${inputUsername}
    Input Text    ${inputUsername}    ${usernameToInput}

Input Password
    [Arguments]    ${passwordToInput}
    Wait Until Page Contains Element    ${inputPassword}
    Input Text    ${inputPassword}    ${passwordToInput}

Input First Name
    [Arguments]    ${firstNameToInput}
    Wait Until Page Contains Element    ${inputFirstName}
    Input Text    ${inputFirstName}    ${firstNameToInput}

Input Family Name
    [Arguments]    ${familyNameToInput}
    Wait Until Page Contains Element    ${inputFamilyName}
    Input Text    ${inputFamilyName}    ${familyNameToInput}

Input Phone Number
    [Arguments]    ${phoneNumberToInput}
    Wait Until Page Contains Element    ${inputPhoneNumber}
    Input Text    ${inputPhoneNumber}    ${phoneNumberToInput}

Click On Lgin Button
    Wait Until Element Is Clickable And Click    ${loginSubmitButton}

Validate User Information
    [Documentation]    This keyword validates information on User Information Page
    ...                 It takes 4 arguments, username to validate, first name to validate, last name to validate and phone number to validate
    [Arguments]    ${usernameToValidate}    ${firstNameToValidate}    ${lastnameToValidate}    ${phoneNumberToValidate}
    Wait Until Page Contains Element    ${userInformationHeader}
    ${usernameValueText}    Get Text    ${usernameValue}
    Should Be Equal    ${usernameValueText}    ${usernameToValidate}
    ${firstNameValueText}    Get Text    ${firstnameValue}
    Should Be Equal    ${firstNameValueText}    ${firstNameToValidate}
    ${lastNameValueText}    Get Text    ${lastnameValue}
    Should Be Equal    ${lastNameValueText}    ${lastnameToValidate}
    ${phoneNumberValueText}    Get Text    ${phoneNumberValue}
    Should Be Equal    ${phoneNumberValueText}    ${phoneNumberToValidate}

Logout From App
    ${loginButtonPresent}    Run Keyword And Return Status    Wait Until Page Contains Element    ${logoutButton}
    IF    ${loginButtonPresent} == True
        Click Element    ${logoutButton}
    END
