*** Settings ***

Resource                ${EXECDIR}/Resource/Common.resource

*** Keywords ***

Launch Browser Window
    [Documentation]    This keyword launches browser with given URL and maximizes window
    Open Browser    ${appURL}    ${browser}
    Maximize Browser Window

Close Browser Window
    [Documentation]    This keyword closes browser
    Close Browser