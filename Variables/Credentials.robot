*** Variables ***

#credentials
${username}                         ABC
${password}                         UserPass@123
${firstName}                        DEF
${familyName}                       XYZ
${phoneNumber}                      123456789
${updateDataDict}                   {"firstname": "PQR", "phone":"987654"}
${usernameUI}                       TUV
${passwordUI}                       UserPass@456
${firstNameUI}                      GHI
${familyNameUI}                     JKL
${phoneNumberUI}                    456123
