*** Variables ***

#Environment variables
${appServerPath}                    ${EXECDIR}/../test-automation-task-main/
${appURL}                           http://localhost:8080/
${browser}                          chrome

#xpath
${demoAppHomeButton}                //a[contains(text(),'Demo app')]
${registerButton}                   //a[contains(text(),'Register')]
${registerHeader}                   //h1[contains(text(),'Register')]
${inputUsername}                    //input[@id='username']
${inputPassword}                    //input[@id='password']
${inputFirstName}                   //input[@id='firstname']
${inputFamilyName}                  //input[@id='lastname']
${inputPhoneNumber}                 //input[@id='phone']
${registerSubmitButton}             //input[@type='submit']
${loginButton}                      //a[contains(text(),'Log In')]
${loginSubmitButton}                //input[@type="submit"]
${userInformationHeader}            //*[contains(text(),'User Information')]
${usernameValue}                    //td[@id='username']
${firstnameValue}                   //td[@id='firstname']
${lastnameValue}                    //td[@id='lastname']
${phoneNumberValue}                 //td[@id='phone']
${logoutButton}                     //a[contains(text(),'Log Out')]
${loginHeader}                      //h1[contains(text(),'Log In')]