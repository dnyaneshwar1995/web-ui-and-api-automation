# README #

This repository contains Web UI and API testcases

# Libraries used #
1. Python requests
2. Python json
3. Robot Framework SeleniumLibrary
4. Robot Framework Collections

# Execution Instructions #
1. Start the Flasky demo app by running run.bat file from App/Flasky-master
2. Start the RF execution by running run.bat file in the execution directory
3. Output files will be generated outside the execution directory in 'out' folder

# Web UI testcase #
1. Register and login as a user

# API test cases #

1. Review users registered in system
2. Get personal information of users with authentication
3. Update personal information of users with authentication