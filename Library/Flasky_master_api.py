import requests
from requests.auth import HTTPBasicAuth
import json

class Flasky_master_api:
	def __init__(self, API_url):
		self.API_url = API_url

	def get_token(self, username, password):
		token_url = self.API_url + 'api/auth/token'
		basic = HTTPBasicAuth(username, password)
		r = requests.get(token_url, headers={"Content-Type":"application/json"},  auth=basic)
		resp_dict = r.json()
		token = resp_dict['token']
		return token

	def review_users(self, token):
		review_user_url = self.API_url + 'api/users'
		r = requests.get(review_user_url, headers={"Content-Type":"application/json", "token": token})
		resp_dict = r.json()
		users = resp_dict['payload']
		return users

	def get_personal_information_of_user(self, token, user):
		get_information_url = self.API_url + 'api/users/' + user
		r = requests.get(get_information_url, headers={"Content-Type":"application/json", "token": token})
		resp_dict = r.json()
		user_data = resp_dict["payload"]
		return user_data

	def update_personal_information_of_user(self, token, user, information_dict):
		get_information_url = self.API_url + 'api/users/' + user
		update_info_header = {"Content-Type":"application/json", "token": token}
		r = requests.put(get_information_url, headers = update_info_header, json=information_dict)
		resp_dict = r.json()
		return resp_dict
